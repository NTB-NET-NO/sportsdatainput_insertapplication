﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Domain.Classes;

namespace SportsDataInsert.InsertApplication.Interfaces.DataMappers
{
    interface ISeasonDataMapper
    {
        List<Season> GetSeasonsBySportId(int sportId);
        void ActivateSeason(int seasonId);
        void DeActivateSeason(int seasonId);
        void UpdateSeasonWithDiscipline(int seasonId, int disciplineId);
    }
}
