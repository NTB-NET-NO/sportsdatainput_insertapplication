using Glue;
using District = NTB.SportsDataInsert.Services.NFF.NFFProdService.District;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.Mappers.NFF
{
    public class DistrictMapper : BaseMapper<District, Domain.Classes.District>
    {
        protected override void SetUpMapper(Mapping<District, Domain.Classes.District> mapper)
        {
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.DistrictName, y => y.DistrictName);
        }
    }
}
