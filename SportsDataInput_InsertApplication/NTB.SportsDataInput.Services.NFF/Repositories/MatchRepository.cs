﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsDataInsert.Services.NFF.Interfaces;
using NTB.SportsDataInsert.Services.NFF.NFFProdService;

namespace NTB.SportsDataInsert.Services.NFF.Repositories
{
    public class MatchRepository : IRepository<Match>, IDisposable, IMatchDataMapper
    {
        public TournamentServiceClient ServiceClient = new TournamentServiceClient();

        internal static readonly ILog Logger = LogManager.GetLogger(typeof(MatchRepository));

        public MatchRepository()
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new TournamentServiceClient();
                
            }

            if (ServiceClient.ClientCredentials == null) return;

            ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
        }

        public int InsertOne(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Match> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Match> GetAll()
        {
            throw new NotImplementedException();
        }

        public Match Get(int id)
        {
            return ServiceClient.GetMatch(id, true, true, true, true);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
        
        public List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults,
                                           bool includeEvents)
        {

            try
            {
                return ServiceClient.GetMatchesByTournament(tournamentId, includeSquad, includeReferees, includeResults,
                                                            includeEvents, null).ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Error(exception.InnerException.Message);
                    Logger.Error(exception.InnerException.StackTrace);
    
                }
                return new List<Match>();
            }
        }

        public List<Match> GetTodaysMatches(DateTime matchDate)
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new TournamentServiceClient();
                
            }

            if (ServiceClient.ClientCredentials != null)
            {
                ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            return ServiceClient.GetMatchesByDate(matchDate, false, false, false, false).ToList();
        }
    }
}
