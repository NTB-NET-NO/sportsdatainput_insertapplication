﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataInsert.Services.NIF.Interfaces;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;

namespace NTB.SportsDataInsert.Services.NIF.Repositories
{
    public class CountyRepository : IRepository<Region>, IDisposable, ICountyDataMapper
    {
        

        /// <summary>
        /// The region service v2 client.
        /// </summary>
        private readonly RegionServiceClient _regionServiceClient = new RegionServiceClient();

        public CountyRepository()
        {
            if (_regionServiceClient.ClientCredentials == null)
            {
                return;
            }

            _regionServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            _regionServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                (servicePointSender, certificate, chain, sslPolicyErrors) => true; // Ignoring cert

            
            
        }

        public int InsertOne(Region domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Region> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Region domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Region domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Region> GetAll()
        {
            throw new NotImplementedException();
        }

        public Region Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Region> GetCounties()
        {
            EmptyRequest4 request = new EmptyRequest4();
            
            RegionsResponse response = _regionServiceClient.GetCounties(request);

            List<Region> counties = response.Regions.ToList();

            return counties;
        }
    }
}
