﻿using Glue;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.Mappers.NIF
{
    public class MunicipalityMapper : BaseMapper<Region, Municipality>
    {
        protected override void SetUpMapper(Mapping<Region, Municipality> mapper)
        {
            mapper.Relate(x=>x.RegionId, y=>y.MunicipalityId);
            mapper.Relate(x=>x.RegionName, y=>y.MuniciaplityName);
        }
    }
}
