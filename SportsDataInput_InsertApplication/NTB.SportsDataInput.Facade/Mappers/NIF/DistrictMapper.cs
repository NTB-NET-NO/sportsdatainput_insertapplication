﻿using Glue;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.Mappers.NIF
{
    public class DistrictMapper : BaseMapper<Region, District>
    {
        protected override void SetUpMapper(Mapping<Region, District> mapper)
        {
            mapper.Relate(x => x.RegionId, y => y.DistrictId);
            mapper.Relate(x => x.RegionName, y => y.DistrictName);
        }
    }
}
