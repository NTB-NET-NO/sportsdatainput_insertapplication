﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;

namespace NTB.SportsDataInsert.Services.NIF.Interfaces
{
    public interface IFederationDataMapper
    {
        // Now we must get the federations and find out who has results defined at NIF.
        List<Federation> GetFederations();
        Federation GetFederation(int id);
        Federation GetFederationByOrgId(int orgId);
    }
}
