﻿using Glue;
using NTB.SportsDataInsert.Services.NFF.NFFProdService;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.Mappers.NFF
{
    class ContactPersonMapper : BaseMapper<OrganizationPerson, ContactPerson>
    {
        protected override void SetUpMapper(Mapping<OrganizationPerson, ContactPerson> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.SurName, y => y.LastName);
            mapper.Relate(x => x.RoleId, y => y.RoleId);
            mapper.Relate(x => x.RoleName, y => y.RoleName);
            mapper.Relate(x => x.MobilePhone, y => y.MobilePhone);
            mapper.Relate(x => x.TeamId, y => y.TeamId);
            mapper.Relate(x => x.TeamName, y => y.TeamName);
            mapper.Relate(x => x.Email, y => y.Email);
        }
    }
}
