﻿using System.Collections.Generic;

namespace NTB.SportsDataInsert.Domain.Classes
{
    public class TournamentListViewModel
    {
        public int SportId { get; set; }
        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }
        public List<Tournament> Tournaments { get; set; }
    }
}
