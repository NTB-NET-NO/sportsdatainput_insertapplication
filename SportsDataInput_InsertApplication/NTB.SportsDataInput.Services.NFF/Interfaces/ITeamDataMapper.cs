﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NFF.NFFProdService;

namespace NTB.SportsDataInsert.Services.NFF.Interfaces
{
    public interface ITeamDataMapper
    {
        List<Team> GetTeamsByMunicipality(int municipalityId);
        List<OrganizationPerson> GetTeamPersons(int teamId);
    }
}
