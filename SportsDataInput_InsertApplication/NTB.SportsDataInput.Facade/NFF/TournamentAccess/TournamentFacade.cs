﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataInsert.Domain.Classes;
using NTB.SportsDataInsert.Facade.Mappers.NFF;
using NTB.SportsDataInsert.Facade.NFF.TournamentInterface;
using NTB.SportsDataInsert.Services.NFF.Interfaces;
using NTB.SportsDataInsert.Services.NFF.Repositories;
using Municipality = NTB.SportsDataInsert.Services.NFF.NFFProdService.Municipality;
using log4net;

namespace NTB.SportsDataInsert.Facade.NFF.TournamentAccess
{

    public class TournamentFacade : ITournamentFacade
    {
        private readonly ITournamentDataMapper _tournamentDataMapper;
        private readonly IDistrictDataMapper _districtDataMapper;
        private readonly IMunicipalityDataMapper _municipalityDataMapper;
        private readonly ISeasonDataMapper _seasonDataMapper;
        private readonly IAgeCategoryDataMapper _ageCategoryDataMapper;
        private readonly IMatchDataMapper _matchDataMapper;

        // Using the repository interface
        private readonly IRepository<Municipality> _municipalityRepository;
        private readonly ITeamDataMapper _teamDataMapper;

        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentFacade));

        public TournamentFacade()
        {
            _tournamentDataMapper = new TournamentRepository();
            _districtDataMapper = new DistrictRepository();
            _municipalityDataMapper = new MunicipalityRepository();
            _seasonDataMapper = new SeasonRepository();
            _ageCategoryDataMapper = new AgeCategoryRepository();
            _teamDataMapper = new TeamRepository();
            _matchDataMapper = new MatchRepository();


            // Repository code
            _municipalityRepository = new MunicipalityRepository();
        }

        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            var result = _tournamentDataMapper.GetTournamentByMunicipalities(municipalities, seasonId);

            var mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }


        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            var t2 = new List<Tournament>();
            try
            {
                var result = _tournamentDataMapper.GetTournamentsByDistrict(districtId, seasonId);

                var mapper = new TournamentMapper();

                var tournaments = result.Select(row => mapper.Map(row, new Tournament())).ToList();

                
                foreach (Tournament tournament in tournaments)
                {
                    var t1 = tournament;
                    t1.SportId = 16;

                    t2.Add(t1);
                }

                return t2;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return t2;
            }
        }

        public List<Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            var result = _tournamentDataMapper.GetTournamentsByTeam(teamId, seasonId);

            TournamentMapper mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }


        public List<District> GetDistricts()
        {
            var districts = _districtDataMapper.GetDistricts();

            var districtMapper = new DistrictMapper();

            return districts.Select(row => districtMapper.Map(row, new District())).ToList();
        }

        public District GetDistrict(int id)
        {
            var result = _districtDataMapper.GetDistrict(id);

            var districtMapper = new DistrictMapper();

            return districtMapper.Map(result, new District());
        }

        public List<Domain.Classes.Municipality> GetMunicipalities()
        {
            var result = _municipalityRepository.GetAll();

            var municipalityMapper = new MunicipalityMapper();

            return result.Select(row => municipalityMapper.Map(row, new Domain.Classes.Municipality())).ToList();
        }

        public List<Domain.Classes.Municipality> GetMunicipalitiesbyDistrict(int districtId)
        {
            var results = _municipalityDataMapper.GetMunicipalitiesbyDistrict(districtId);

            var municipalityMapper = new MunicipalityMapper();

            return results.Select(row => municipalityMapper.Map(row, new Domain.Classes.Municipality())).ToList();
        }

        public List<Tournament> GetClubsByTournament(int tournamentId, int seasonId)
        {
            throw new NotImplementedException();
        }

        public List<Season> GetSeasons()
        {
            SeasonMapper mapper = new SeasonMapper();

            var result = _seasonDataMapper.GetSeasons();

            return result.Select(row => mapper.Map(row, new Season())).ToList();
        }

        public Season GetSeason(int id)
        {
            var result = _seasonDataMapper.GetSeason(id);

            var mapper = new SeasonMapper();

            return mapper.Map(result, new Season());
        }

        public Season GetOngoingSeason()
        {
            var result = _seasonDataMapper.GetOngoingSeason();

            var mapper = new SeasonMapper();

            return mapper.Map(result, new Season());
        }


        public List<AgeCategory> GetAgeCategoriesTournament()
        {
            var mapper = new AgeCategoryMapper();
            var result = _ageCategoryDataMapper.GetAgeCategoriesTournament();

            return result.Select(row => mapper.Map(row, new AgeCategory())).ToList();
        }

        public List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad,
                                                                 bool includeReferees, bool includeResults,
                                                                 bool includeEvents)
        {
            var mapper = new MatchMapper();

            var result = _matchDataMapper.GetMatchesByTournament(tournamentId, includeSquad, includeReferees,
                                                                 includeResults, includeEvents);
            return result.Select(row => mapper.Map(row, new Match())).ToList();

        }

        public AgeCategory GetAgeCategoryTournament()
        {
            throw new NotImplementedException();
        }

        public List<Team> GetTeamsByMunicipality(int municipalityId, int seasonId)
        {
            throw new NotImplementedException();
        }

        public Tournament GetTournament(int tournamentId)
        {
            var result = _tournamentDataMapper.GetTournament(tournamentId);

            var mapper = new TournamentMapper();

            return mapper.Map(result, new Tournament());
        }

        public List<Match> GetTodaysMatches(DateTime matchDate)
        {
            var result = _matchDataMapper.GetTodaysMatches(matchDate);

            var mapper = new MatchMapper();

            return result.Select(row => mapper.Map(row, new Match())).ToList();
        }

        public List<ContactPerson> GetTeamContacts(int teamId)
        {
            var result = _teamDataMapper.GetTeamPersons(teamId);

            var mapper = new ContactPersonMapper();

            return result.Select(row => mapper.Map(row, new ContactPerson())).ToList();
        }
    }
}