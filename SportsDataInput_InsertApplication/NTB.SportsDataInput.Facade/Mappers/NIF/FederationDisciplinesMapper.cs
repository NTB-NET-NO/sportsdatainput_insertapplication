using Glue;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;
using NTB.SportsDataInsert.Common;

namespace NTB.SportsDataInsert.Facade.Mappers.NIF
{
    public class FederationDisciplinesMapper : BaseMapper<FederationDiscipline, Domain.Classes.FederationDiscipline>
    {
        protected override void SetUpMapper(Mapping<FederationDiscipline, Domain.Classes.FederationDiscipline> mapper)
        {
            mapper.Relate(x => x.ActivityCode, y=> y.ActivityCode);
            mapper.Relate(x => x.ActivityId, y => y.ActivityId);
            mapper.Relate(x => x.ActivityName, y => y.ActivityName);
        }
    }
}
