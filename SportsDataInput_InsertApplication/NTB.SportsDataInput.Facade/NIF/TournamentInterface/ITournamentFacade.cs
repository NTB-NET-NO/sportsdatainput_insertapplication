﻿using System;
using System.Collections.Generic;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.NIF.TournamentInterface
{
    public interface ITournamentFacade
    {
        // Returns list
        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId);
        List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId);
        List<Tournament> GetTournamentsByTeam(int teamId, int seasonId);

        List<Match> GetTodaysMatches(DateTime matchDate);
    }
}
