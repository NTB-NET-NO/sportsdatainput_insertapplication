﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataInsert.Domain.Classes;
using SportsDataInsert.InsertApplication.Interfaces.Common;
using SportsDataInsert.InsertApplication.Interfaces.DataMappers;
using log4net;

namespace SportsDataInsert.InsertApplication.DataMappers.Populators
{
    public class PopulateDataMapper : IRepository<Populator>, IDisposable, IPopulatorDataMapper 
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PopulateDataMapper));

        public int InsertOne(Populator domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Populator> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Populator domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Populator domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Populator> GetAll()
        {
            throw new NotImplementedException();
        }

        public Populator Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void InsertPopulateStart()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertPopulateStart", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@DateTimeStart", DateTime.Now));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
                
            }
        }

        public void UpdatePopulateEnd()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_UpdatePopulateEnd", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@DateTimeEnd", DateTime.Now));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

            }
        }

        public void UpdatePopulateStart()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_UpdatePopulateStart", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@DateTimeStart", DateTime.Now));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

            }
        }

        public Populator GetPopulate()
        {
            var populate = new Populator();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetPopulateDateTime", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }


                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["PopulationStart"] != DBNull.Value)
                        {
                            populate.Start = Convert.ToDateTime(sqlDataReader["PopulationStart"]);
                        }
                        else
                        {
                            populate.Start = null;
                        }

                        if (sqlDataReader["PopulationEnd"] != DBNull.Value)
                        {
                            populate.End = Convert.ToDateTime(sqlDataReader["PopulationEnd"]);
                        }
                        else
                        {
                            populate.End = null;
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
                return populate;
            }
        }
    }
}
