﻿using System;
using System.Collections.Generic;

namespace NTB.SportsDataInsert.Domain.Classes
{
    public class UserDistrictMapper
    {
        /// <summary>
        ///     Get or set the District Id
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        ///     Get or set the User Id
        /// </summary>
        public List<Guid> UserIds { get; set; }

        /// <summary>
        ///     Gets or sets the Work Date 
        /// </summary>
        public DateTime WorkDate { get; set; }
    }
}
