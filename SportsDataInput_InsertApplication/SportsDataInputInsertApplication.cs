﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using NTB.SportsDataInsert.Domain.Classes;
using NTB.SportsDataInsert.Facade.NFF.TournamentAccess;
using NTB.SportsDataInsert.Facade.NIF.SportAccess;
using SportsDataInsert.InsertApplication.DataMappers.Districts;
using SportsDataInsert.InsertApplication.DataMappers.Populators;

namespace SportsDataInsert.InsertApplication
{
    public partial class SportsDataInputInsertApplication : Form
    {
        public SportsDataInputInsertApplication()
        {
            InitializeComponent();
        }

        private List<District> GetDistricts()
        {
            var datamapper = new DistrictDataMapper();
            return new List<District>(datamapper.GetAll());
        }

        public void GetRemoteMatches()
        {
            List<District> districts = GetDistricts();

            GetRemoteMatchesForDistrict(districts);

            // Insert into database
        }

        private void GetRemoteMatchesForDistrict(List<District> districts)
        {
            // first we do the soccer matches
            var soccerFacade = new TournamentFacade();

            // Setting up the sport facade
            var sportsFacade = new SportFacade();

            var soccerSeason = soccerFacade.GetOngoingSeason();

            // Creating a doneEvents Reset Event Array
            ManualResetEvent[] doneEvents = new ManualResetEvent[districts.Count];

            // Creating a counter to help us with creating the threads
            int counter = 0;

            // Creating a list of threads, so we know what each one is doing
            List<Thread> threads = new List<Thread>();

            foreach (District district in districts)
            {
                if (district.DistrictId == 0) continue;

                doneEvents[counter] = new ManualResetEvent(false);

                var districtMatches = new DistrictMatches(doneEvents[counter])
                {
                    District = district,
                    SoccerFacade = soccerFacade,
                    SportSeason = soccerSeason,
                    SportsFacade = sportsFacade
                };

                Thread matchThread = new Thread(districtMatches.GetDistrictMatches)
                    {
                        Name = "districtThread_" + district.DistrictName
                    };
                matchThread.Start();

                threads.Add(matchThread);
                // dictionary.Add(district, matches);

                counter++;
            }

            foreach (ManualResetEvent doneEvent in doneEvents)
            {
                doneEvent.WaitOne();
            }

        }

        private void startupTimer_Tick(object sender, EventArgs e)
        {
            startupTimer.Stop();

            // Setting the database - checking if we are to update or insert
            var dataMapper = new PopulateDataMapper();
            var populator = dataMapper.GetPopulate();

            for (int i = this.Controls.Count-1; i >= 0; i--)
            {
                if (!(this.Controls[i] is Label)) continue;
                Controls[i].Text = @"Databasen blir oppdatert.";
                Controls[i].Center();
            }

            if (populator == null || populator.Start == null)
            {
                dataMapper.InsertPopulateStart();
            }
            else
            {
                dataMapper.UpdatePopulateStart();
            }
            // Starting Remote getting
            GetRemoteMatches();

            dataMapper.UpdatePopulateEnd();

            for (int i = this.Controls.Count-1; i >= 0; i--)
            {
                if (!(this.Controls[i] is Label)) continue;
                Controls[i].Text = @"Database er blitt oppdatert. Denne applikasjonen vil nå avsluttes";
                Controls[i].Center();
            }

            Thread.Sleep(30000);

            this.Close();
            this.Dispose();
        }

        private void SportsDataInputInsertApplication_Load(object sender, EventArgs e)
        {
            startupTimer.Enabled = true;

            startupTimer.Start();
        }

        private void SportsDataInputInsertApplication_Shown(object sender, EventArgs e)
        {
            Thread.Sleep(1500);
            Label startupLabel = new Label { Text = @"Vi starter opp Sports Data Insert", AutoSize = true };
            Controls.Add(startupLabel);
            startupLabel.Center();
            
        }
    }
}
