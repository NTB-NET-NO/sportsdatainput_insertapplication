﻿using Glue;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.Mappers.NIF
{
    public class AgeCategoryMapper : BaseMapper<ClassCode, AgeCategory>
    {
        protected override void SetUpMapper(Mapping<ClassCode, AgeCategory> mapper)
        {
            mapper.Relate(x => x.ClassId, y => y.CategoryId);
            mapper.Relate(x => x.Name, y => y.CategoryName);
            mapper.Relate(x => x.ToAge, y => y.MaxAge);
            mapper.Relate(x => x.FromAge, y => y.MinAge);
            mapper.Relate(x => x.ActivityId, y => y.SportId);
        }
    }
}
