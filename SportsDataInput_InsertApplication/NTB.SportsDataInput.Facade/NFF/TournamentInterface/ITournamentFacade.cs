using System;
using System.Collections.Generic;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.NFF.TournamentInterface
{
    interface ITournamentFacade
    {
        // Returns list
        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId);
        List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId);
        List<Tournament> GetClubsByTournament(int tournamentId, int seasonId);
        List<Tournament> GetTournamentsByTeam(int teamId, int seasonId);
        Tournament GetTournament(int tournamentId);

        List<District> GetDistricts();
        District GetDistrict(int id);

        List<Municipality> GetMunicipalities();
        List<Municipality> GetMunicipalitiesbyDistrict(int districtId);

        List<Season> GetSeasons();
        Season GetSeason(int id);
        Season GetOngoingSeason();

        List<AgeCategory> GetAgeCategoriesTournament();
        AgeCategory GetAgeCategoryTournament();

        List<Team> GetTeamsByMunicipality(int municipalityId, int seasonId);

        List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees,
                                                          bool includeResults, bool includeEvents);

        List<Match> GetTodaysMatches(DateTime matchDate);

        List<ContactPerson> GetTeamContacts(int teamId);
    }
}
