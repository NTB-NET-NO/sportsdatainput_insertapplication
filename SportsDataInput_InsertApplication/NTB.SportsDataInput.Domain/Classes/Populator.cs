﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataInsert.Domain.Classes
{
    public class Populator
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }
}
