﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;

namespace NTB.SportsDataInsert.Services.NIF.Interfaces
{
    public interface ITeamDataMapper
    {
        List<Person> GetTeamPersons(int teamId);
    }
}
