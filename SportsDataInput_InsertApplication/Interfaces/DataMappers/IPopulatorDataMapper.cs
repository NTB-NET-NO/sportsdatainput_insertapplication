﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataInsert.Domain.Classes;

namespace SportsDataInsert.InsertApplication.Interfaces.DataMappers
{
    interface IPopulatorDataMapper
    {
        void InsertPopulateStart();
        void UpdatePopulateStart();
        void UpdatePopulateEnd();
        Populator GetPopulate();

    }
}
