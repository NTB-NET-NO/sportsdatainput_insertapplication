﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Domain.Classes;

namespace SportsDataInsert.InsertApplication.Interfaces.DataMappers
{
    interface IMunicipalityDataMapper
    {
        List<Municipality> GetMunicipalitiesByJobId(int jobId);
        List<Municipality> GetMunicipalitiesBySportId(int sportId);
        void StoreMunicipalities(List<Municipality> municipalities);
        void DeleteAll();

    }
}
