using Glue;
using Tournament = NTB.SportsDataInsert.Services.NFF.NFFProdService.Tournament;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.Mappers.NFF
{
    public class TournamentMapper : BaseMapper<Tournament, Domain.Classes.Tournament>
    {
        protected override void SetUpMapper(Mapping<Tournament, Domain.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.AgeCategoryId, y=>y.AgeCategoryId);
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.GenderId, y => y.GenderId);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentNumber, y => y.TournamentNumber);
            mapper.Relate(x => x.PushChanges, y => y.Push); 
            mapper.Relate(x => x.Division, y => y.Division);
            
        }
    }
}
