using Glue;
using NTB.SportsDataInsert.Common;
using Municipality = NTB.SportsDataInsert.Services.NFF.NFFProdService.Municipality;

namespace NTB.SportsDataInsert.Facade.Mappers.NFF
{
    class MunicipalityMapper : BaseMapper<Municipality, Domain.Classes.Municipality>
    {
        protected override void SetUpMapper(Mapping<Municipality, Domain.Classes.Municipality> mapper)
        {
            mapper.Relate(x => x.Id, y=> y.MunicipalityId);
            mapper.Relate(x => x.Name, y => y.MuniciaplityName);
        }
    }
}