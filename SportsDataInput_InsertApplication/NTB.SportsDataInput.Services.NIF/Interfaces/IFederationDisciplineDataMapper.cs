﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;

namespace NTB.SportsDataInsert.Services.NIF.Interfaces
{
    public interface IFederationDisciplineDataMapper
    {
        List<FederationDiscipline> GetFederationDisciplines(int orgId);
        FederationDiscipline GetFederationDiscipline(int id);
    }
}
