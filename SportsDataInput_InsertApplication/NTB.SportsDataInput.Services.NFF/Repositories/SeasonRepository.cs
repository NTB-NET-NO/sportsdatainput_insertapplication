﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using NTB.SportsDataInsert.Services.NFF.Interfaces;
using NTB.SportsDataInsert.Services.NFF.NFFProdService;

namespace NTB.SportsDataInsert.Services.NFF.Repositories
{
    public class SeasonRepository : IRepository<Season>, IQueryable, ISeasonDataMapper
    {
        /// <summary>
        /// The service client.
        /// </summary>
        public MetaServiceClient ServiceClient = new MetaServiceClient();

        public int InsertOne(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Season> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Season> GetAll()
        {
            throw new NotImplementedException();
        }

        public Season Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public Expression Expression { get; private set; }
        public Type ElementType { get; private set; }
        public IQueryProvider Provider { get; private set; }

        public List<Season> GetSeasons()
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new MetaServiceClient();
                
            }

            if (ServiceClient.ClientCredentials != null)
            {
                ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            return new List<Season>(ServiceClient.GetSeasons());
        }

        public Season GetSeason(int id)
        {
            return GetSeasons().Single(s => s.SeasonId == id);
        }


        public Season GetOngoingSeason()
        {
            var seasons = GetSeasons();

            //var currentSeason = seasons.Last(s => Convert.ToDateTime(s.SeasonStartDate).Year == DateTime.Today.Year
            //                              ||
            //                              Convert.ToDateTime(s.SeasonEndDate).Year == DateTime.Today.Year &&
            //                              s.SeasonNameLong.Contains(DateTime.Today.Year.ToString(CultureInfo.InvariantCulture)));

            var currentSeason = seasons.Last(s => Convert.ToDateTime(s.SeasonStartDate).Year == DateTime.Today.Year &&
                                              Convert.ToDateTime(s.SeasonEndDate).Year == DateTime.Today.Year);
            // return GetSeasons().Last(

            return currentSeason;

        }
    }
}
