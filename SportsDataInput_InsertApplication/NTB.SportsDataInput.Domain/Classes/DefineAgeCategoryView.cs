﻿using System.Collections.Generic;

namespace NTB.SportsDataInsert.Domain.Classes
{
    public class DefineAgeCategoryView
    {
        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }

        public List<Season> Seasons { get; set; }
    }
}
