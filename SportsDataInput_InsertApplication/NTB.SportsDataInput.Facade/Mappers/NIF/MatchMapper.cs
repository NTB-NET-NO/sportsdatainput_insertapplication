﻿using Glue;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.Mappers.NIF
{
    public class MatchMapper : BaseMapper<TournamentMatchExtended, Match>
    {
        protected override void SetUpMapper(Mapping<TournamentMatchExtended, Match> mapper)
        {
            mapper.Relate(x => x.Awayteam, y => y.AwayTeam);
            mapper.Relate(x => x.AwayteamId, y => y.AwayTeamId);
            mapper.Relate(x => x.Hometeam, y => y.HomeTeam);
            mapper.Relate(x => x.HometeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.MatchDate, y => y.MatchDate);
            mapper.Relate(x => x.MatchStartTime , y => y.MatchStartTime);
        }
    }
}
