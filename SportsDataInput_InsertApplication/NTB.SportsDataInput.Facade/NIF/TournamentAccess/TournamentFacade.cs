﻿using System;
using System.Collections.Generic;
using NTB.SportsDataInsert.Domain.Classes;
using NTB.SportsDataInsert.Facade.NIF.TournamentInterface;

namespace NTB.SportsDataInsert.Facade.NIF.TournamentAccess
{
    public class TournamentFacade : ITournamentFacade
    {
        public TournamentFacade()
        {
        }

        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Match> GetTodaysMatches(DateTime matchDate)
        {
            return new List<Match>();
        }
    }
}
