﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NFF.NFFProdService;

namespace NTB.SportsDataInsert.Services.NFF.Interfaces
{
    public interface IDistrictDataMapper
    {
        List<District> GetDistricts();
        District GetDistrict(int districtId);
    }
}
