﻿using Glue;
using NTB.SportsDataInsert.Common;
using Season = NTB.SportsDataInsert.Services.NIF.NIFProdService.Season;

namespace NTB.SportsDataInsert.Facade.Mappers.NIF
{
    public class SeasonMapper : BaseMapper<Season, Domain.Classes.Season>
    {
        protected override void SetUpMapper(Mapping<Season, Domain.Classes.Season> mapper)
        {
            mapper.Relate(x=>x.SeasonId, y=>y.SeasonId);
            mapper.Relate(x => x.SeasonName, y => y.SeasonName);
            mapper.Relate(x => x.FromDate, y => y.SeasonStartDate);
            mapper.Relate(x => x.ToDate, y => y.SeasonEndDate);
        }
    }
}