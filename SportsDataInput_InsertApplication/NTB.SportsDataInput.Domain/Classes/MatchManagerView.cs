﻿using System.Collections.Generic;

namespace NTB.SportsDataInsert.Domain.Classes
{
    public class MatchManagerView
    {
        public List<District> Districts { get; set; }

        public List<UserProfile> UserProfiles { get; set; }

        public Dictionary<District, List<Match>> DistrictMatches { get; set; }
    }
}
