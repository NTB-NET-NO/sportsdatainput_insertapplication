﻿using Glue;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;
using Person = NTB.SportsDataInsert.Services.NIF.NIFProdService.Person;

namespace NTB.SportsDataInsert.Facade.Mappers.NIF
{
    public class PersonMapper : BaseMapper<Person, ContactPerson>
    {
        protected override void SetUpMapper(Mapping<Person, ContactPerson> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.HomeAddress.PhoneHome, y => y.HomePhone);
            mapper.Relate(x => x.HomeAddress.PhoneMobile, y => y.MobilePhone);
            mapper.Relate(x => x.HomeAddress.PhoneWork, y => y.OfficePhone);
            mapper.Relate(x => x.HomeAddress.Email, y => y.Email);
            
        }
    }
}
