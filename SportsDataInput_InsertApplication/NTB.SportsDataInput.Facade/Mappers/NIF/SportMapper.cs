using Glue;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.Mappers.NIF
{
    public class SportMapper : BaseMapper<FederationDiscipline, Sport>
    {
        protected override void SetUpMapper(Mapping<FederationDiscipline, Sport> mapper)
        {
            mapper.Relate(x => x.ActivityName, y=>y.Name);
            mapper.Relate(x => x.ActivityId, y => y.Id);
        }
    }
}


