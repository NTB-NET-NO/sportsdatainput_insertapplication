﻿namespace NTB.SportsDataInsert.Domain.Classes
{
    public class Person
    {
        public int PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneHome { get; set; }

        public string PhoneMobile { get; set; }

        public string PhoneWork { get; set; }

        public string Email { get; set; }

        public string Gender { get; set; }
    }
}
