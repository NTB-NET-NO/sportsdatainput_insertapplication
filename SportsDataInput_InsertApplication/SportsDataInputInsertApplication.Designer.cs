﻿namespace SportsDataInsert.InsertApplication
{
    partial class SportsDataInputInsertApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.startupTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // startupTimer
            // 
            this.startupTimer.Interval = 30000;
            this.startupTimer.Tick += new System.EventHandler(this.startupTimer_Tick);
            // 
            // SportsDataInputInsertApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 262);
            this.Name = "SportsDataInputInsertApplication";
            this.Text = "Sports Data Input Insert Application";
            this.Load += new System.EventHandler(this.SportsDataInputInsertApplication_Load);
            this.Shown += new System.EventHandler(this.SportsDataInputInsertApplication_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer startupTimer;
    }
}

