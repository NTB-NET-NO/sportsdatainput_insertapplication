﻿using System;
using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;

namespace NTB.SportsDataInsert.Services.NIF.Interfaces
{
    public interface IMatchDataMapper
    {
        List<TournamentMatchExtended> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId);
        List<TournamentMatch> GetTodaysMatches(DateTime matchDate, int seasonId);
    }
}
