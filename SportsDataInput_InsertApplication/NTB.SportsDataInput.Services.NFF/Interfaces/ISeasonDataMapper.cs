﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NFF.NFFProdService;

namespace NTB.SportsDataInsert.Services.NFF.Interfaces
{
    public interface ISeasonDataMapper
    {
        List<Season> GetSeasons();
        Season GetSeason(int id);
        Season GetOngoingSeason();
    }
}
