﻿using NTB.SportsDataInsert.Services.NIF.NIFProdService;

namespace NTB.SportsDataInsert.Services.NIF.Interfaces
{
    public interface IPersonDataMapper
    {
        Person GetPersonById(int id);
    }
}
