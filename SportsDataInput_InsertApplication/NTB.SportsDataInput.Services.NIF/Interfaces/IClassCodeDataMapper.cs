﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;

namespace NTB.SportsDataInsert.Services.NIF.Interfaces
{
    public interface IClassCodeDataMapper
    {
        List<ClassCode> GetClassCodes(int orgId);
    }
}
