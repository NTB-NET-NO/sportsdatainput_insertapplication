﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;

namespace NTB.SportsDataInsert.Services.NIF.Interfaces
{
    public interface ICountyDataMapper
    {
        List<Region> GetCounties();
    }
}
