﻿using System;
using System.Collections.Generic;
using NTB.SportsDataInsert.Domain.Classes;

namespace SportsDataInsert.InsertApplication.Interfaces.DataMappers
{
    interface IDistrictDataMapper
    {
        List<District> GetDistrictsByUserId(Guid userId);
        void InsertUserDistrictMap(Guid userId, int districtId);
    }
}
