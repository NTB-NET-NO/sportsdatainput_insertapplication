﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NIF.NIFProdService;

namespace NTB.SportsDataInsert.Services.NIF.Interfaces
{
    public interface IFunctionDataMapper
    {
        List<Function> GetTeamFunctions(int teamId);
    }
}
