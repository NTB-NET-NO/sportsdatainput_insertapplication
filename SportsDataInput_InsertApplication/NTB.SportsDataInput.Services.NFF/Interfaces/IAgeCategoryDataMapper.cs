﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Services.NFF.NFFProdService;

namespace NTB.SportsDataInsert.Services.NFF.Interfaces
{
    public interface IAgeCategoryDataMapper
    {
        List<AgeCategoryTeam> GetAgeCategoriesTeam();
        List<AgeCategoryTournament> GetAgeCategoriesTournament();
    }
}
