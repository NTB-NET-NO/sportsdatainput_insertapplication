﻿using System.Collections.Generic;
using NTB.SportsDataInsert.Domain.Classes;

namespace SportsDataInsert.InsertApplication.Interfaces.DataMappers
{
    interface IMatchDataMapper
    {
        List<Match> GetMatchesByTournamentId(int tournamentId);
        List<Match> GetLocalMatchesByDistrictId(int districtId);
    }
}
