using Glue;
using Season = NTB.SportsDataInsert.Services.NFF.NFFProdService.Season;
using NTB.SportsDataInsert.Common;
using NTB.SportsDataInsert.Domain.Classes;

namespace NTB.SportsDataInsert.Facade.Mappers.NFF
{
    public class SeasonMapper : BaseMapper<Season, Domain.Classes.Season>
    {
        protected override void SetUpMapper(Mapping<Season, Domain.Classes.Season> mapper)
        {
            mapper.Relate(x => x.SeasonId, y=>y.SeasonId);
            mapper.Relate(x => x.SeasonNameLong, y => y.SeasonName);
            mapper.Relate(x => x.SeasonStartDate, y => y.SeasonStartDate);
            mapper.Relate(x => x.SeasonEndDate, y => y.SeasonEndDate);
            mapper.Relate(x => x.Ongoing, y=>y.SeasonActive);
        }
    }
}