﻿using System.Collections.Generic;
using System.Linq;

namespace SportsDataInsert.InsertApplication.Interfaces.Common
{
    public interface IRepository<T>
    {
        int InsertOne(T domainobject);
        void InsertAll(List<T> domainobject);
        void Update(T domainobject);
        void Delete(T domainobject);
        IQueryable<T> GetAll();
        T Get(int id);
    }
}
