﻿namespace SportsDataInsert.InsertApplication.Interfaces.DataMappers
{
    interface ISportDataMapper
    {
        int GetSportIdBySeasonId(int seasonId);
        int GetSportIdFromOrgId(int orgId);
    }
}
